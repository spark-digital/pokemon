# First Dojo: TDD Requirements

## Pokemon class

### Stage 1
* Pokemon should have a name.
* Pokemon should be named as it's type if no name is given
* Pokemon has health points
* Pokemon has 3 different actions.
* Actions must have speed.
* Pokemon has speed.
* Pokemon has an element.
* Pokemon has a type.
* Pokemon can receive damage.
* Pokemon can perform actions.
* Pokemon can die.
* Pokemon can alert its hp are low
* If a pokemon has an advantage against another one considering its type, attacks cause more damage.
* If a pokemon has an advantage against another one considering its type, damage received could be less.

## BattleManager class

### Stage 1
* Players must be able to choose the action the pokemon will perform this round.
* BattleManager should choose a random action to perform.
* BattleManager should be able to determine which action is performed first based on action speed and pokemon speed. If action speed A is greater than B, then A is first, if they are tied, faster pokemon will go first, else random.
* An attack can be used if it has remaining “power points” (remaining attack)
* If health points of a pokemon goes below zero, battle ends
* If health points of a pokemon goes below certain value (percentage of total health points), an alert is triggered.