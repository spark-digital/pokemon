# Dojo Description

## How it works

* Dojo sessions will have a regular frequency (weekly)
* A group of sessions will be focused on working to resolve iterative requirements for a particular problem working on pairs or small groups.
* Roles and Responsibilities:
    * Sensei (moderator): define the exercise and provide explanation and guidance about it.
    * Participants: resolve the requirement explained by the sensei, discuss solutions and explain implementations.
* Develop the objective with the dojos
* After the exercise is done, the results will be shared among the team members.
* The outcome will be shared with the community (it can be a short article/abstract/recommendation).

## Rules

* All assistants must participate
* All participants write code
* Sensei doesn't give answers
* Work on the requirements inside the session time

## Initial requirements

* Have a GitHub account
* Have Visual Studio Code installed in PC
* Have LiveShare extension installed in VS Code