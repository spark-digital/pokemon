import Pokemon from '../src/Pokemon'

describe('Pokemon', () => { 
  describe('Name assertions', () => {
    it('Pokemon should have a name', ()=> {
      //arrange 
  
      //act
      const pokemon = new Pokemon({name: 'some name'});
  
      //assert
      expect(pokemon.name).toBe('some name');
    });

    it("Pokemon should be named as it's type if no name is given", () => {
      //arrange
  
      //act
  
      //assert
    })
    
  })
});